drop table dna_sch_01.employee;

create table dna_sch_01.employee (
     id        serial primary key
    ,name      varchar(255)
    ,age       integer
    ,salary    float
    ,address   varchar(255)
);

insert into dna_sch_01.employee (name, age, salary, address) values ('Dus', 40, 1000.99, 'This is one address');
insert into dna_sch_01.employee (name, age, salary, address) values ('Jean-Claude', 30, 2000.99, 'This is another address');
insert into dna_sch_01.employee (name, age, salary, address) values ('Yoda', 80, 2000.99, 'This yoda address');

select * from dna_sch_01.employee;