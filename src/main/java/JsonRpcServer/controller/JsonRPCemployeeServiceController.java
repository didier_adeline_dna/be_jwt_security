package JsonRpcServer.controller;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import JsonRpcServer.controller.dto.EmployeeDto;
import JsonRpcServer.controller.dto.IDDto;

import java.io.IOException;


@JsonRpcService("/JsonRPCDemo2")
public interface JsonRPCemployeeServiceController {

    String helloWorld(@JsonRpcParam(value = "name") String name);

    Integer createEmployee(@JsonRpcParam(value = "employee") EmployeeDto employee) throws IOException;
    EmployeeDto readEmployee(@JsonRpcParam(value = "name") String employeeName);
    String updateEmployee(@JsonRpcParam(value = "employee") EmployeeDto employee) throws IOException;
    IDDto deleteEmployee(@JsonRpcParam(value = "name") String employeeName);
    String resetPassword(@JsonRpcParam(value = "name") String name,
                         @JsonRpcParam(value = "password") String password) throws IOException;

}