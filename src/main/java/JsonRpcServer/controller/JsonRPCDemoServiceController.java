package JsonRpcServer.controller;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import JsonRpcServer.controller.dto.DsrcInvDocAttrDto;


@JsonRpcService("/JsonRpcServer.JsonRPCDemo")
public interface JsonRPCDemoServiceController {

    String helloWorld(@JsonRpcParam(value = "name") String typeId);
//    DocTypeDto getByType(@JsonRpcParam(value="type") String type);
//    String createType(@JsonRpcParam(value="type") String type,
//                       @JsonRpcParam(value="tableRef") String tableRef);

//    String documentsList(@JsonRpcParam(value = "documents") String docId);

    DsrcInvDocAttrDto getByDocId(@JsonRpcParam(value = "docId") Long docId);

//    DsrcInvDocAttrDto getJsonByDocId(@JsonRpcParam(value = "docId") Long docId ,
//            @JsonRpcParam(value = "veniceInvoiceNumber") String veniceInvoiceNumber,
//            @JsonRpcParam(value = "aaxCustomerId") String aaxCustomerId,
//            @JsonRpcParam(value = "billDocNumber") String billDocNumber,
//            @JsonRpcParam(value = "serviceCountry") String serviceCountry,
//            @JsonRpcParam(value = "invoiceIssueDate") String invoiceIssueDate,
//            @JsonRpcParam(value = "billingPeriodFrom") String billingPeriodFrom,
//            @JsonRpcParam(value = "billingPeriodTo") String billingPeriodTo,
//            @JsonRpcParam(value = "salesPartnerName") String salesPartnerName,
//            @JsonRpcParam(value = "paymentReference") String paymentReference,
//            @JsonRpcParam(value = "totalNet") String totalNet,
//            @JsonRpcParam(value = "totalGross") String totalGross,
//            @JsonRpcParam(value = "currency") String currency
//    );

}