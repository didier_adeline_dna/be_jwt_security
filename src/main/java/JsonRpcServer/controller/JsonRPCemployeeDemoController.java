package JsonRpcServer.controller;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import JsonRpcServer.controller.dto.EmployeeDto;
import JsonRpcServer.controller.dto.IDDto;
import JsonRpcServer.services.IEmployeeService;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@AutoJsonRpcServiceImpl
public class JsonRPCemployeeDemoController implements JsonRPCemployeeServiceController {

    private static final Logger logger = Logger.getLogger(JsonRPCemployeeDemoController.class);

    private final IEmployeeService employeeService;

    public JsonRPCemployeeDemoController(IEmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public String helloWorld(String name) {
        logger.debug("helloWorld");
        return "Hello " + name + "!";
    }

    @Override
    public Integer createEmployee(EmployeeDto employee) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String employeeJSON =mapper.writeValueAsString(employee);

        logger.debug( "Creating employee : "  + employeeJSON );

        return employeeService.create(employee);
    }

    @Override
    public EmployeeDto readEmployee(String employeeName) {
        logger.debug( "Reading employee using name : "  + employeeName);
        return employeeService.read(employeeName);
    }

    @Override
    public String updateEmployee(EmployeeDto employee) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String employeeJSON = mapper.writeValueAsString(employee);

        logger.debug( "Updating employee : "  + employeeJSON );

        return employeeService.update(employee);
    }

    @Override
    public IDDto deleteEmployee(String employeeName) {
        logger.debug( "Deleting employee : "  + employeeName );

        return employeeService.delete(employeeName);
    }

    @Override
    public String resetPassword(String name, String password) throws IOException {
        logger.debug( "Reset employee password request: "  + password );
        return  employeeService.resetPassword(name, password);

//                "Request password change for user "+name +" to " + password + "!";
    }

}