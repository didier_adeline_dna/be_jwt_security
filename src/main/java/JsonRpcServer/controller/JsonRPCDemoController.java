package JsonRpcServer.controller;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import JsonRpcServer.controller.dto.DsrcInvDocAttrDto;
import JsonRpcServer.repository.dao.DsrcInvDocAttrDao;
import JsonRpcServer.services.IDsrcInvListService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
@AutoJsonRpcServiceImpl
public class JsonRPCDemoController implements JsonRPCDemoServiceController {

    private static final Logger logger = Logger.getLogger(JsonRPCDemoController.class);

//    private final IDocTypeService docTypeService;
    private final IDsrcInvListService dsrcService;


    public JsonRPCDemoController(IDsrcInvListService dsrcService) {
//        this.docTypeService = docHubService;
        this.dsrcService = dsrcService;
    }


    @Override
    public String helloWorld(String name) {
        logger.debug("helloWorld");
        return "Hello " + name + "!";
    }

//    @Override
//    public String documentsList(String  docId) {
//        DsrcDTO dto = new DsrcDTO();
//        DsrcInvDocAttrDao dao = new DsrcInvDocAttrDao();
//
//        dao.setBillDocNumber(docId);
//        dto.
//
//    };

    public DsrcInvDocAttrDto getJsonByDocId(Long docId) {
        return getByDocId(docId);
    }


    @Override
    public DsrcInvDocAttrDto getByDocId(Long docId) {
//        DsrcInvDocAttrDao dao =  IDsrInvDocAttrRepository repo.;
//        logger.debug("== LongId: "+docId);
        return dsrcService.getByDocId(docId);
    };

    private DsrcInvDocAttrDto setDocDto(DsrcInvDocAttrDao dao) {
        DsrcInvDocAttrDto dto = new DsrcInvDocAttrDto();

        dto.setDocId(dao.getDocId());
        dto.setVeniceInvoiceNumber(dao.getVeniceInvoiceNumber());
        dto.setAaxCustomerId(dao.getAaxCustomerId());

        dto.setBillDocNumber(dao.getBillDocNumber());
        dto.setInvoiceIssueDate(dao.getInvoiceIssueDate());
        dto.setBillingPeriodFrom(dao.getBillingPeriodFrom());
        dto.setBillingPeriodTo(dao.getBillingPeriodTo());

        dto.setCurrency(dao.getCurrency());
        dto.setTotalGross(dao.getTotalGross());
        dto.setTotalNet(dao.getTotalNet());

        dto.setServiceCountry(dao.getServiceCountry());
        dto.setSalesPartnerId(dao.getSalesPartnerId());
        dto.setSalesPartnerName(dao.getSalesPartnerName());
        dto.setPaymentReference(dao.getPaymentReference());




        return dto;
    };


//    @Override
//    public String createType(String type, String tableRef) {
//        logger.debug("Creating type:"+type+ "tableRef:"+tableRef);
//        return docTypeService.createType(type, tableRef);
//    }
//
//   @Override
//    public DocTypeDto getByType(String _type) {
//        logger.debug("Reading docType using doc_type  : " + _type);
//        return docTypeService.getByType(_type);
//    }


}