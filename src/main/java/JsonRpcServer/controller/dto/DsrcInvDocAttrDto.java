package JsonRpcServer.controller.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonPropertyOrder({"docId", "veniceInvoiceNumber", "aaxCustomerId", "billDocNumber", "serviceCountry",
        "invoiceIssueDate", "billingPeriodFrom", "billingPeriodTo", "salesPartnerId", "salesPartnerName",
        "paymentReference", "totalNet", "totalGross", "currency" })
public class DsrcInvDocAttrDto implements Serializable {

    private Long docId;

    private String veniceInvoiceNumber;
    private String aaxCustomerId;
    private String billDocNumber;

    private String serviceCountry;
    private String invoiceIssueDate;
    private String billingPeriodFrom;

    private String billingPeriodTo;
    private String salesPartnerId;
    private String salesPartnerName;

    private String paymentReference;
    private Double totalNet;
    private Double totalGross;

    private String currency;


}
