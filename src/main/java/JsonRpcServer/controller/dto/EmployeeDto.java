package JsonRpcServer.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class EmployeeDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String name;
    private Integer age;
    private Long salary;
    private String address;
    private String password;

}