package JsonRpcServer.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class IDDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Integer id;

}