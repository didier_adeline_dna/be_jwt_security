package JsonRpcServer.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class DocTypeDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long typeId;
    private String type;
    private String tableRef;

}