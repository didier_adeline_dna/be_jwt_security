package JsonRpcServer.controller;

import JsonRpcServer.services.IEmployeeService;
import JsonRpcServer.services.JwtUserDetailsService;
import JsonRpcServer.services.implementation.EmployeeServiceImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;


@RestController

public class HelloController {


    private final IEmployeeService employeeService;

    public HelloController(IEmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping({"/hello/dad"})
    public String truc() {
        return "You are the boss";
    };


    @RequestMapping({"/hello"})
//    public String hello() {
    public String hello(@RequestHeader HttpHeaders headers){
        System.out.println("Header Content-Type: "+headers.get("Content-Type"));
        System.out.println("Header Authorization: "+headers.get("Authorization"));
        return "Hello World";
    }

    @RequestMapping({"/encode"})
    public String test(@RequestParam(required = true) String id)  {
          return "encodePwd for "+id+" Encoded:\n"+'"'+employeeService.encodePwd(id)+'"'+"\n";      }


}
