package JsonRpcServer.services.implementation;

import JsonRpcServer.controller.dto.DsrcInvDocAttrDto;
import JsonRpcServer.repository.IDsrInvDocAttrRepository;
import JsonRpcServer.repository.dao.DsrcInvDocAttrDao;
import JsonRpcServer.services.IDsrcInvListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
// This is necessary when using deleteByName
@Transactional
public class DsrcInvListServiceImpl implements IDsrcInvListService {

    private static final Logger logger = Logger.getLogger(DsrcInvListServiceImpl.class);

    @Autowired
    private final IDsrInvDocAttrRepository docRepo;

    public DsrcInvListServiceImpl(IDsrInvDocAttrRepository docRepo) {
        this.docRepo = docRepo;
    };

    @Override
    public DsrcInvDocAttrDto getByDocId(Long docId) {

        if (docRepo.existsById(docId)) {
            logger.debug("*** Yes, document with docId "+docId+" exists.");
        }
        else {
            logger.debug("*** NOOOOOOO");
        }

        DsrcInvDocAttrDao dao = docRepo.findById(docId).get();  //new DsrcInvDocAttrDao();
        DsrcInvDocAttrDto dto = new DsrcInvDocAttrDto();

        dto.setDocId(dao.getDocId());
        dto.setAaxCustomerId(dao.getAaxCustomerId());
        dto.setBillDocNumber(dao.getBillDocNumber());
        dto.setBillingPeriodFrom(dao.getBillingPeriodFrom());
        dto.setBillingPeriodTo(dao.getBillingPeriodTo());
        dto.setCurrency(dao.getCurrency());
        dto.setInvoiceIssueDate(dao.getInvoiceIssueDate());
        dto.setPaymentReference(dao.getPaymentReference());
        dto.setSalesPartnerId(dao.getSalesPartnerId());
        dto.setSalesPartnerName(dao.getSalesPartnerName());
        dto.setServiceCountry(dao.getServiceCountry());
        dto.setVeniceInvoiceNumber(dao.getVeniceInvoiceNumber());
        dto.setTotalNet(dao.getTotalNet());
        dto.setTotalGross(dao.getTotalGross());
        return dto;
    }
}