package JsonRpcServer.services.implementation;

import JsonRpcServer.config.WebSecurityConfig;
import JsonRpcServer.controller.dto.EmployeeDto;
import JsonRpcServer.controller.dto.IDDto;
import JsonRpcServer.repository.EmployeeRepository;
import JsonRpcServer.repository.dao.EmployeeDao;
import JsonRpcServer.services.IEmployeeService;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
// This is necessary when using deleteByName
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {

    private static final Logger logger = Logger.getLogger(EmployeeServiceImpl.class);


    @Autowired
    private final EmployeeRepository employeeRepository;
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }



    @Override
    public Integer create(EmployeeDto employeeDto) {
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.setName(employeeDto.getName());
        employeeDao.setAge(employeeDto.getAge());
        employeeDao.setSalary(employeeDto.getSalary());
        employeeDao.setAddress(employeeDto.getAddress());
        employeeDao.setPassword(employeeDto.getPassword());
//        EmployeeDao responseDao = emp
//        ponseDao.getId());
        return employeeRepository.save(employeeDao).getId();
    }

    @Override
    public EmployeeDto read(String name) {

        logger.debug("Running the read service");

      if (employeeRepository.existsByName(name)) {
        EmployeeDao employeeDao = employeeRepository.getByName(name);
        EmployeeDto employeeDtoToReturn = new EmployeeDto();
        ModelMapper modelMapper = new ModelMapper();

        employeeDtoToReturn.setName(employeeDao.getName());
        employeeDtoToReturn.setAge(employeeDao.getAge());
        employeeDtoToReturn.setSalary(employeeDao.getSalary());
        employeeDtoToReturn.setAddress(employeeDao.getAddress());
        employeeDtoToReturn.setPassword(employeeDao.getPassword());

          return employeeDtoToReturn;
      } else {
          EmployeeDto employeeDtoToReturn = new EmployeeDto();
          employeeDtoToReturn.setName("error");
          employeeDtoToReturn.setAge(0);
          employeeDtoToReturn.setSalary((long) 0);
          employeeDtoToReturn.setAddress("error");
          employeeDtoToReturn.setPassword("");
          return employeeDtoToReturn;
      }
    }

    @Override
    public String encodePwd(String pwd) {
        BCryptPasswordEncoder encode = new BCryptPasswordEncoder();
//        encode.encode(pwd);
        logger.debug("Clear: "+pwd+"Encoded: "+encode.encode(pwd));
        return encode.encode(pwd);
    }

    @Override
    public String update(EmployeeDto employeeDto) {
        logger.debug("Running the update service");

        // The save() method will create a new entry if it does not exist and update it if it
        // exists. The existence is based on the object passed and the existence of the primary
        // key of course. This is why we first read the DAO from the database here for the example
        // the DAO will then have the DB values, but will also have the ID
        // saving will then not create a new entry. Without doing this, if the DAO is not recovered
        // a new entry will be inserted when calling the save function
        EmployeeDao employeeDao = employeeRepository.getByName(employeeDto.getName());

        employeeDao.setName(employeeDto.getName());
        employeeDao.setAge(employeeDto.getAge());
        employeeDao.setSalary(employeeDto.getSalary());
        employeeDao.setAddress(employeeDto.getAddress());

        EmployeeDao employeeDaoUpdated = employeeRepository.save(employeeDao);
        String stringToReturn = "Employee updated in the database, it has ID : " + employeeDaoUpdated.getId().toString();
        return stringToReturn;

    }

    @Override
    public String resetPassword(String name,String pwd) {

        EmployeeDao employeeDao = employeeRepository.getByName(name);
        logger.debug("---"+employeeDao.getName()+"<-- "+employeeDao.getPassword());
        employeeDao.setPassword(encodePwd(pwd));
        EmployeeDao employeeDaoUpdated = employeeRepository.save(employeeDao);
        logger.debug("---"+employeeDaoUpdated.getName()+"--> "+employeeDaoUpdated.getPassword());
        return "Employee password changed for : " + employeeDaoUpdated.getName();
    }

    @Override
    public IDDto delete(String name) {

        logger.debug("Running the delete service with name : " + name );

        // there are at least two options to delete by name
        // get the dto by its name then use the delete method that accepts a DTO and deletes it
        EmployeeDao employeeDao = employeeRepository.getByName(name);
        logger.debug( "Deleting Employee with name : " + employeeDao.getName().toString());
        //employeeRepository.delete(employeeDao);

        // or use the deleteByName that accepts a string as the name to delete by name
        // not this deleteByName needs the @Transactional at the service above
        employeeRepository.deleteByName(name);

        IDDto returnID = new IDDto(employeeDao.getId());
        return returnID;

    }

}