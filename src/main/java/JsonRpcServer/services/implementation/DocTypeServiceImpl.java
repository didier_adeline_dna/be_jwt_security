package JsonRpcServer.services.implementation;

import JsonRpcServer.controller.dto.DocTypeDto;
import JsonRpcServer.repository.DocTypeRepository;
import JsonRpcServer.repository.dao.DocTypeDao;
import JsonRpcServer.services.IDocTypeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
// This is necessary when using deleteByName
@Transactional
public class DocTypeServiceImpl implements IDocTypeService {

    private static final Logger logger = Logger.getLogger(DocTypeServiceImpl.class);

    @Autowired
    private final DocTypeRepository doctypeRepository;

    public DocTypeServiceImpl(DocTypeRepository doctypeRepository) {
            this.doctypeRepository = doctypeRepository;
    }


    @Override
    public String createType(String type,String tableRef) {

        logger.debug("Running the create service");

        DocTypeDao doctypeDao = new DocTypeDao();
        doctypeDao.setType(type);
        doctypeDao.setTableRef(tableRef);

        DocTypeDao doctypeDaoCreated = doctypeRepository.save(doctypeDao);
        long x = doctypeDaoCreated.getTypeId();
//        doctypeDao.setTypeId(x);
//        return this.getByTypeId(x);
        return String.valueOf(x);
    }

    @Override
    public DocTypeDto getByType(String docType) {
        logger.debug("Running the read service");

        if (doctypeRepository.existsByType(docType)) {
            DocTypeDao doctypeDao = doctypeRepository.getByType(docType);
            DocTypeDto doctypeToReturn = new DocTypeDto();

            doctypeToReturn.setTypeId(doctypeDao.getTypeId());
            doctypeToReturn.setType(doctypeDao.getType());
            doctypeToReturn.setTableRef(doctypeDao.getTableRef());
            logger.debug("==> "+doctypeDao.getTypeId()+" ** "+
                                doctypeDao.getType()+" ** "+
                                doctypeToReturn.getTableRef());

            return doctypeToReturn;
        } else {
            DocTypeDto doctypeToReturn = new DocTypeDto();
            doctypeToReturn.setTypeId((long) 0);
            doctypeToReturn.setType("error");
            doctypeToReturn.setTableRef("error");
            return doctypeToReturn;
        }
    }

//    @Override
    public DocTypeDto getByTypeId(Long typeId) {
        logger.debug("Running the search by typeId service "+typeId);

        if (doctypeRepository.existsByTypeId(typeId)) {
            DocTypeDao doctypeDao = doctypeRepository.getByTypeId(typeId);
            DocTypeDto doctypeToReturn = new DocTypeDto();

            doctypeToReturn.setTypeId(doctypeDao.getTypeId());
            doctypeToReturn.setType(doctypeDao.getType());
            doctypeToReturn.setTableRef(doctypeDao.getTableRef());
            return doctypeToReturn;
        } else {
            DocTypeDto doctypeToReturn = new DocTypeDto();
            doctypeToReturn.setTypeId((long) 0);
            doctypeToReturn.setType("error");
            doctypeToReturn.setTableRef("error");
            return doctypeToReturn;
        }

    }



    }
