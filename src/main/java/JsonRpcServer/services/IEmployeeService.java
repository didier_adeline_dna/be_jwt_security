package JsonRpcServer.services;

import JsonRpcServer.controller.dto.EmployeeDto;
import JsonRpcServer.controller.dto.IDDto;

public interface IEmployeeService {

    Integer create(EmployeeDto employeeDto);
    EmployeeDto read(String name);
    String update(EmployeeDto employeeDto);
    IDDto delete(String name);
    String encodePwd(String pwd);
    String resetPassword(String name,String pwd);

}