package JsonRpcServer.services;


import JsonRpcServer.controller.dto.DsrcInvDocAttrDto;


public interface IDsrcInvListService {
    DsrcInvDocAttrDto getByDocId(Long docId);
}

