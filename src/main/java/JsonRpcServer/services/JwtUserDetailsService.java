package JsonRpcServer.services;


import java.util.ArrayList;

import JsonRpcServer.controller.dto.EmployeeDto;
import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(JwtUserDetailsService.class);

    private final IEmployeeService employeeService;

    public JwtUserDetailsService(IEmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        UserDetails userDetails = new UserDetails();
        if ("dnauser".equals(username)) {
            return new User("dnauser", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
                    new ArrayList<>());
        }
        else {
            EmployeeDto userDto = employeeService.read(username);
            if (userDto!=null && userDto.getName()!=null)
            logger.debug("User: "+ userDto.getName()+" Pwd:"+userDto.getPassword() );
            return  new User(userDto.getName(),userDto.getPassword(),
                  new ArrayList<>());
        }

    }
}