package JsonRpcServer.services;

import JsonRpcServer.controller.dto.DocTypeDto;

public interface IDocTypeService {

    String createType(String type, String tableRef);
    DocTypeDto getByType(String docType);
//    IDDto delete(String type);

}