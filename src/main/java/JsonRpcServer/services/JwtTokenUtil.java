package JsonRpcServer.services;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -2550185165626007488L;
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    @Value("${jwt.secret}")
    private String secret;

//retrieve username from jwt token
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieveing any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

//check if the token has expired
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

//generate token for user
    public String generateToken(UserDetails userDetails) {
/**
 *  The generateToken claims can get additional kv like the 'Test' item below
 *  <p>
 *  {
 *  "sub": "Dus",
 *   "Test": "DNA Claims item",
 *   "exp": 1584311584,
 *   "iat": 1584293584
 *  }
 *  <p>
 *  use  https://jwt.io/
 */

        Map<String, Object> claims = new HashMap<>();
        /*ToDo: remove messing wiyh claims */
        claims.keySet().forEach(k -> System.out.println("@@@Claims["+k+"]: "+claims.get(k)));
        claims.put("Test",((String) "DNA Claims item") );

        System.out.println("===Claims size: "+claims.size());
        Set<String> ks = claims.keySet();
        Object key = null;
        Iterator<String> ite = ks.iterator();

        String item = "";
        while (ite.hasNext()) {
//        for (int i=0; i < ks.size(); i++) {
            item  = ite.next();
            System.out.println("@@@ Claims["+item+"]: "+claims.get(item));
        }

        return doGenerateToken(claims, userDetails.getUsername());
    }

//while creating the token -
//1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
//2. Sign the JWT using the HS512 algorithm and secret key.
//3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
//   compaction of the JWT to a URL-safe string

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

//validate token
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

}
