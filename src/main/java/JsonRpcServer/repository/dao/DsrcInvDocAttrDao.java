package JsonRpcServer.repository.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity


@Table(name = "dsrc_inv_doc_attr")
public class DsrcInvDocAttrDao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "doc_id")
    Long docId;

    @Column(name = "venice_invoice_number")
    private String veniceInvoiceNumber;

    @Column(name = "aax_customer_id")
    private String aaxCustomerId;

    @Column(name = "bill_doc_number")
    private String billDocNumber;

    @Column(name = "service_country")
    private String serviceCountry;

    @Column(name = "invoice_issue_date")
    private String invoiceIssueDate;

    @Column(name = "billing_period_from")
    private String billingPeriodFrom;

    @Column(name = "billing_period_to")
    private String billingPeriodTo;

    @Column(name = "sales_partner_id")
    private String salesPartnerId;

    @Column(name = "sales_partner_name")
    private String salesPartnerName;

    @Column(name = "payment_reference")
    private String paymentReference;

    @Column(name = "total_net")
    private Double totalNet;

    @Column(name = "total_gross")
    private Double totalGross;

    @Column(name = "currency")
    private String currency;

/*    public Long findById(Long docId) {
    }*/
    //FROM CSV ////////// END


//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "dsrcInvDocAttr")
//    private DsrcInvMappingFileDao dsrcInvMappingFile;
}
