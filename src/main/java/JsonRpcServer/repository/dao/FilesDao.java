package JsonRpcServer.repository.dao;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "files")
@NoArgsConstructor
@AllArgsConstructor
public class FilesDao implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private Long fileId;

    @Column(name = "file_name")
    private String fileName;

    @Lob
    @Column(name = "file")
    private String file;

//    @ManyToOne//(cascade = CascadeType.ALL)
//    @JoinColumn(name = "docId")
////    @JsonBackReference
//    private DocumentsDao documentsDao;
//
//    public FilesDao(String fileName, String file, DocumentsDao documentsDao){
//        this.fileName = fileName;
//        this.file = file;
//        this.documentsDao = documentsDao;
//    }
}
