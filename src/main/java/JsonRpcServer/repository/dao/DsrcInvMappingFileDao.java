//package demo.JsonRpcServer.repository.dao;
//
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.time.LocalDateTime;
//
//import static demo.JsonRpcServer.repository.dao.StateEnum.INCOMPLETE;
//
//@Data
//@Entity
//@Table(name = "dsrc_inv_mapping_file")
//@NoArgsConstructor
//@AllArgsConstructor
//public class DsrcInvMappingFileDao implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "map_file_id")
//    private Long mapFileId;
//
//    @Column(name = "file_name")
//    private String fileName;
//
//    @Lob
//    @Basic(fetch = FetchType.LAZY)
//    @Column(name = "file")
//    private String file;
//
//    @Column(name = "state")
//    private StateEnum state;
//
//    @Column(name = "number_of_pdfs")
//    private Integer numberOfPdfs;
//
//    @Column(name = "processed_pdfs")
//    private Integer processedPdfs;
//
//    @Column(name = "create_timestamp")
//    private LocalDateTime createTimestamp = LocalDateTime.now();
//
////    @OneToMany(mappedBy = "mapFileId", cascade = CascadeType.ALL)
////    @JsonBackReference
////    private List<DocumentsDao> documentsDaos;
//
//    public DsrcInvMappingFileDao(String fileName, String file, Integer numberOfPdfs ){
//        this.fileName = fileName;
//        this.file = file;
//        this.numberOfPdfs = numberOfPdfs;
//        this.state = INCOMPLETE;
//        this.processedPdfs = 0;
//    }
//}
