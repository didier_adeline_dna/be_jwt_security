package JsonRpcServer.repository.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity

@Table(name = "employee")

public class EmployeeDao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="employee_id_seq",sequenceName="employee_id_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="employee_id_seq")
    Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "salary")
    private Long salary;

    @Column(name = "address")
    private String address;

    @Column(name = "password")
    private String password;

}