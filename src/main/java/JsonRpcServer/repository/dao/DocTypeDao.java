package JsonRpcServer.repository.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity

@Table(name = "doc_types") //, schema = "dhub")

public class DocTypeDao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long typeId;

    @Column(name = "type")
    private String type;

    @Column(name = "table_ref")
    private String tableRef;

//    @Column(name="create_user")
//    private  String createUser;
//
//    @Column(name="create_timestamp")
//    private  timeStamp;



}