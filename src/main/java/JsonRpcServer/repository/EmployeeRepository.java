package JsonRpcServer.repository;

import JsonRpcServer.repository.dao.EmployeeDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeDao, Integer> {
    boolean existsByName(String name);
    EmployeeDao getByName(String name);
    EmployeeDao save(EmployeeDao employeeDao);
    void delete(EmployeeDao employeeDao);
    void deleteByName(String name);

}