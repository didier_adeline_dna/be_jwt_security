package JsonRpcServer.repository;

import JsonRpcServer.repository.dao.FilesDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IFilesDao extends JpaRepository<FilesDao, Long> {
    FilesDao getByFileName(String fileName);
    boolean existsByFileName(String fileName);
        Optional<FilesDao> findByFileId(Long fileId);
}
