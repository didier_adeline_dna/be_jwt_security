package JsonRpcServer.repository;

import JsonRpcServer.repository.dao.DsrcInvDocAttrDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDsrInvDocAttrRepository extends JpaRepository<DsrcInvDocAttrDao, Long > {




//    DocTypeDao createType(DocTypeDto typeDto);
//    DocTypeDao getByName(String type);
//    DsrcInvDocAttrDao getByDocId(String docId);

    DsrcInvDocAttrDao getByDocId(Long docId);

//    DocTypeDao getByTypeId(Long type);
//    boolean existsByType(String type);
//    boolean existsByTypeId(Long typeId);


}