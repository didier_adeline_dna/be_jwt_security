//package demo.JsonRpcServer.repository;
//
//import demo.JsonRpcServer.repository.dao.DsrcInvMappingFileDao;
//import demo.JsonRpcServer.repository.dao.StateEnum;
//import org.springframework.data.jpa.JsonRpcServer.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface IDsrcInvMappingFileDao extends JpaRepository<DsrcInvMappingFileDao, Long> {
//    List<DsrcInvMappingFileDao> getByState(StateEnum state);
//    boolean existsByState(StateEnum state);
//
//    DsrcInvMappingFileDao getById(Long docId);
//    boolean existsById(Long docId);
//}
