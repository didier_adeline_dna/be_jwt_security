package JsonRpcServer.repository;

import JsonRpcServer.repository.dao.DocTypeDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocTypeRepository extends JpaRepository<DocTypeDao, Long> {

//    DocTypeDao createType(DocTypeDto typeDto);
//    DocTypeDao getByName(String type);

    DocTypeDao getByType(String type);
    DocTypeDao getByTypeId(Long type);
    boolean existsByType(String type);
    boolean existsByTypeId(Long typeId);
}
