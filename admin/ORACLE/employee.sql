DROP SEQUENCE "DNA_SCH"."EMPLOYE_SEQ";
DROP TRIGGER  "DNA_SCH"."EMPLOYEE_TRG" ;

CREATE SEQUENCE  "DNA_SCH"."EMPLOYE_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999 INCREMENT BY 1 START WITH 1000000000 NOCACHE  ORDER  NOCYCLE  NOPARTITION ;

DROP TABLE "DNA_SCH"."EMPLOYEE";

create table "DNA_SCH".employee (
                                id  number(10)
                                ,name  varchar2(255)
                                ,age	number(3)
                                ,salary number(10.2)
                                ,address varchar2(255)
                                ,constraint pk_employee_id primary key (id)
);


CREATE OR REPLACE  TRIGGER "DNA_SCH"."EMPLOYEE_TRG"
    before insert or update on EMPLOYEE
    for each row
begin
    :new.id := nvl(:new.id, EMPLOYEE.nextval);
end EMPLOYEE_TRG;

----grant select, insert, update, delete on BRPYC.VM_SNMP_IMPORT to read4splunk
