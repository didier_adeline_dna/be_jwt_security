-- Table: dna_sch.employee

--DROP TABLE dna_sch.employee;

CREATE TABLE dna_sch.employee
(
    id  serial primary key,
    name character varying(255) COLLATE pg_catalog."default",
    age integer,
    salary double precision,
    address character varying(255) COLLATE pg_catalog."default",
    info json,
    password character varying(255) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;